#!/bin/bash

# Temp directory for holding files.
tempDir="temp"

# Checking for input file.
if [[ $1 == "" ]]
then
	echo "Must give input image and lowest allowed quality setting! (1-100)"

	exit 1
fi

# Checking for quality flag.
if [[ $2 == "" ]]
then
	echo "Must give lower quality limit! (1-100)"

	exit 1
fi

# Checking if the temp directory name is already taken.
if [ -f "$tempDir" ]
then
	echo "Temp directory $tempDir already exists!"

	exit 1
fi

# Making the temp directory if it doesn't exist.
if [ ! -e "$tempDir" ]
then
	mkdir $tempDir
fi

# Declaring and initializing quality variable.
quality=100

# Starting resolution percentage.
resolution=100

# Lower jpeg quality limit.
qualityLimit=$2

# Copying the input image to a an temp reference.
convert $1 $tempDir/ref.png

# Copying the reference image to a working image.
cp $tempDir/ref.png $tempDir/image.png

# Main loop.
while [ 1 ]
do
	echo -e -n "Trying $resolution% scale with JPEG quality $quality...\r"

	# Converting to a quality.
	convert $tempDir/image.png -strip -quality $quality $tempDir/temp.jpg

	# Getting the size of the jpg.
	size=$(ls -l $tempDir/temp.jpg | awk '{print $5}')

	# Checking if the size is small enough.
	if [ $size -lt 12288 ]
	then
		break
	fi

	# Decrementing the jpg quality if image was too small.
	quality=$(($quality - 1))

	# Checking if the quality is below the lower limit and scaling down the reference image if it is.
	if [ $quality -lt $qualityLimit ]
	then
		resolution=$(($resolution - 10))

		if [[ $resolution < 0 ]]
		then
			echo "Image could not be fit with given settings!"

			exit 1
		fi

		echo -e "\nScaling to $resolution% size and trying again..."

		convert $tempDir/ref.png -scale $resolution% -strip $tempDir/image.png

		quality=100
	fi
done

# Getting the output jpg size.
size=$(ls -l $tempDir/temp.jpg | awk '{print $5}')

# Padding the output jpg with nulls.
xxd -p -l $((12288 - $size)) /dev/zero | xxd -r -p >> $tempDir/temp.jpg

# Encoding the jpg file into ppm image.
echo -e "P6\n64 64\n255" > $tempDir/temp.ppm
cat $tempDir/temp.jpg >> $tempDir/temp.ppm

# Converting the ppm to a png.
convert $tempDir/temp.ppm -strip out.png

# Copying the finished jpeg out of the temp directory.
cp $tempDir/temp.jpg out.jpg

echo -e "\nDone!\nWrote images to:\n\tencoded in skin: out.png\n\tskin: out.jpg"

# Emptying the temp directory.
rm -f $tempDir/*

exit 0
