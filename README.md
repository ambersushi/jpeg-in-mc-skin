## About

A simple shell script for automatically recompressing JPEG images down to fit into the 12.2kb of RGB pixel data in a Minecraft skin.

Skin image:
![Skin](./examples/out.png)

Encoded image:
![Encoded image](./examples/out.jpg)

(Image: Author https://en.wikipedia.org/wiki/de:User:Dnalor_01, Source (Wikimedia Commons), license (CC-BY-SA 3.0))

### Image encoding method

Minecraft skins are 64x64 pixels.
Skin images are 24-bit RGB PNGs.
Each pixel can store 3 bytes of information.
64 x 64 x 3 = 12288 bytes of data.

The input image is lowered in quality and possibly scaled down to fit in the 12kb of data.

## Requirements

`convert` (imagemagick)

## Usage

```bash
./jpgFitter.sh [input_image] [minimum quality (1-100)]
```

`minimum quality` is the lowest allowed JPEG compression quality used before image gets resized to a smaller resolution.

## Roadmap

Add binary search for finding the smallest quality setting instead of linear search.
